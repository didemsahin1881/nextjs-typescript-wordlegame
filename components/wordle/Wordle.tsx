"use client"
import React, { useEffect, useMemo, useState } from 'react'
import styles from './Wordle.module.css'

type wordleType = {
    wordle:string
}
const Wordle = ({wordle}:wordleType) => {
    const [guess, setGuess] = useState<Array<string>>([]);
    const [guessAll, setGuessAll] = useState<Array<Array<string>>>([]);
    const [keyboard, setKeyboard] = useState([
       [ "e", "r", "t", "y", "u", "ı", "o", "p", "ğ", "ü"],
       [ "a", "s", "d", "f", "g", "h", "j", "k", "l", "ş", "i"],
       ["en", "z", "c", "v", "b", "n", "m", "ö", "ç", "bs"] 
    ])
    
    if (wordle.length !== 5) {
        throw new Error('The wordle must have 5 letters...!');
    }
    const charMap = useMemo(() => {
        return wordle.split('').reduce<Record<string,number>>((acc, char) => {
            if (!acc.hasOwnProperty(char)) {
                acc[char] = 1
            }
            else {
                acc[char] += 1 
            }
            return acc
        }, {});
    }, [wordle]);
    useEffect(() => {
        const handleKeyFunct = (e: KeyboardEvent) => {
            //regex
            const letterCntrl = /^[a-z]$/.test(e.key)
            const backspace = e.key === 'Backspace';
            const enter = e.key === 'Enter';
            console.log(e.key);
            if (backspace) {
                setGuess(guess.filter(gss => gss !== guess[guess.length - 1]))
            }
            else if (letterCntrl && guess.length < 5){
                setGuess(prev => [...prev, e.key]);
            }
            else if (enter && guess.length === 5) {
                setGuessAll(prev => [...prev, guess]);
                setGuess([]);
            } 
        }
        window.addEventListener('keydown', handleKeyFunct)
        return () => {
            window.removeEventListener('keydown', handleKeyFunct);
        }
    }, [guess])
    console.log("guess", guess);
    console.log("guessAll", guessAll);

    const addKeyboard = (i:number, j:number) => {
        const letterCntrl = /^[a-z]$/.test(keyboard[i][j]);
            const backspace = keyboard[i][j] === 'bs';
            const enter = keyboard[i][j] === 'en';
            console.log(keyboard[i][j]);
            if (backspace) {
                setGuess(guess.filter(gss => gss !== guess[guess.length - 1]))
            }
            else if (letterCntrl && guess.length < 5){
                setGuess(prev => [...prev, keyboard[i][j]]);
            }
            else if (enter && guess.length === 5) {
                setGuessAll(prev => [...prev, guess]);
                setGuess([]);
            } 
    }

    const isCorrect = guessAll.length > 0 && guessAll[guessAll.length - 1].join('') === wordle;
    const isFailure = !isCorrect && guessAll.length === 6;

  return (
      <div>
          <PreviousGuess guessAll={guessAll} wordle={wordle} charMap = {charMap}></PreviousGuess>

          {!isCorrect && !isFailure &&  <CurrentGuess guess={guess}></CurrentGuess>}
          {
              Array.from({ length: 6 - guessAll.length - (isCorrect ? 0 : 1) }).map((_, i) => {
                  return <NullGuess key={i}></NullGuess>
              })
          }  
          {
              isCorrect && <div style={{ textAlign: 'center', color:'green' }} >Your Guess is Correct !</div>
          }
          {
              isFailure && <div style={{textAlign: 'center', color:'red'}}>You are out of guesses !</div>
          }
         
          <div style={{marginTop:'12px'}}>
              {
                  keyboard.map((row,i) => {
                      return <div className={styles.row} key={i}>
                          {row.map((col, j) => {
                              return <div onClick={() => addKeyboard(i,j)} className={styles.col} key={j}>{col}</div>
                          })}
                      </div>
                  })
              }
              </div> 
    </div>
  )
}
type previousguesssub = {
    all: Array<string>
    wordle: string 
    charMap:Record<string,number>
}
const PreviousGuessSub = ({ all, wordle, charMap }: previousguesssub) => {
    const cMap = {...charMap}
    return (
     <div className={styles.cellContainer} >
          {
                all.map((a, i) => {
                    const wordleLetter = wordle[i];
                    console.log(a);
                    let greenControl = wordleLetter === a
                    let isPr = false;
                    if (!greenControl && cMap[a]) {
                        isPr = true;
                        cMap[a]-= 1;
                    }
                  return <div className={`${styles.cell} ${greenControl ? styles.green : ''} ${isPr ? styles.yellow : ''}`} key={i}>{a}</div>
              })
          }
        </div>
    )
}
type previousguess = {
    guessAll: Array<Array<string>>
    wordle: string
    charMap:Record<string,number>
}
const PreviousGuess = ({guessAll,wordle,charMap}:previousguess) => {
    return (
     <>
          {
              guessAll.map((all, i) => {
                  return <div  key={i}>
                      <PreviousGuessSub wordle={wordle} all={all} charMap={charMap}></PreviousGuessSub>
                  </div>
              })
          }
        </>
    )
}
type currentguess = {
    guess: Array<string>
}
const CurrentGuess = ({guess}:currentguess) => {
    return (
     <div className={styles.cellContainer} >
          {
              Array.from({ length: 5 }).map((_, i) => {
                  return <div className={styles.cell} key={i}>{ guess[i]}</div>
              })
          }
        </div>
    )
}
const NullGuess = () => {
    return (
     <div className={styles.cellContainer} >
          {
              Array.from({ length: 5 }).map((_, i) => {
                  return <div  className={styles.cell} key={i}></div>
              })
          }
        </div>
    )
}
export default Wordle
