import Image from 'next/image'
import Wordle from '../../components/wordle/Wordle'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <Wordle wordle="didem"></Wordle>
 
    </main>
  )
}
